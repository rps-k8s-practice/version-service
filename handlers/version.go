package handlers

import (
	"net/http"
	version "version-service/models/version"

	"github.com/labstack/echo/v4"
)

// Version godoc
// @Summary Return version of application
// @Description Return version of application
// @Accept  json
// @Produce  json
// @Success 200 {object} models.version
// @Router /version [get]
func Version(c echo.Context) error {
	return c.JSON(http.StatusOK, version.New())
}
