package handlers

import (
	"net/http"
	ping "version-service/models/ping"

	"github.com/labstack/echo/v4"
)

// Ping godoc
// @Summary Healthcheck endpoint
// @Description Healthcheck endpoint
// @Accept  json
// @Produce  json
// @Success 200 {object} models.ping
// @Router /ping [get]
func Ping(c echo.Context) error {
	return c.JSON(http.StatusOK, ping.New())
}
