package models

type Version interface{}

type version struct {
	Version string `json:"version"`
}

func New() Version {
	return version{"6.0.1"}
}
