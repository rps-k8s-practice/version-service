package models

type Ping interface{}

type ping struct {
	Ping string `json:"ping"`
}

func New() Ping {
	return ping{"pong"}
}
