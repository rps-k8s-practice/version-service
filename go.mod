module version-service

require (
	github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc
	github.com/labstack/echo/v4 v4.0.0
	github.com/swaggo/echo-swagger/v2 v2.0.0-20190219082602-1a361fc821b8
	github.com/swaggo/swag v1.4.0
)
